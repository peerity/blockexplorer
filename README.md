# Peerity Block Explorer
[peerity.info](https://peerity.info) will be used for Block Explorer.

### Features
- Browse Blocks
- Browse Transactions
- Search Blocks/Transactions
- View Network Info
- Statistics

### Credits
- [CoreUI template](https://github.com/mrholek/CoreUI-Free-Bootstrap-Admin-Template/tree/master/React_Full_Project), [Demo](http://coreui.io/demo/React_Demo/)