import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        Peerity BlockExplorer &copy; 2017 <a href="http://peerity.io">Peerity LLC</a>.
        <span className="float-right"><a href="https://peerity.net">Peerity Network</a></span>
      </footer>
    )
  }
}

export default Footer;
