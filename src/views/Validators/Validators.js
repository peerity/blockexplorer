import React, { Component } from 'react'
// import {Grid, Row, Col, Panel, Table, Media} from 'react-bootstrap/lib'
import {Container, Row, Col, Panel, Table, Media} from 'reactstrap'
// import Breadcrumbs from 'react-breadcrumbs'

import update from 'immutability-helper';
var dateFormat = require('dateformat');
var global = require("../../Global");

export default class Validators extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount(){
        fetch('https://peerity.info/tm/validators')
            .then(result=>result.json())
            .then(data=>this.setState(data))
    }

    render() {
        return (
            <Container>
                <Row>
                    <div className="col-lg-12 p-0">
                    {this.state.result &&
                        <div className="card">
                            <div className="card-header"><i className="fa fa-align-justify"></i> Validators</div>
                            <div className="card-block">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>address</th>
                                            <th>voting_power</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.result[1].validators.map( (item, index) =>
                                            <tr key={item.address}>
                                                <td>{index}</td>
                                                <td>Address: <code>{item.address}</code><br />
                                                    PubKey: <code>{item.pub_key}</code>
                                                </td>
                                                <td>{item.voting_power}</td>
                                            </tr>
                                        )
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    }
                    </div>
                </Row>
            </Container>
        )
    }
}
