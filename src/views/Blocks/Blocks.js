import React, { Component } from 'react';
import {Container, Row, Col, Table} from 'reactstrap'
// import Breadcrumbs from 'react-breadcrumbs'

var dateFormat = require('dateformat');
var global = require("../../Global");

class Blocks extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: []
        };
    }

    componentDidMount(){
        /**
         [
         {
             "id": 2,
             "time": 1493442669000,
             "numTxs": 1,
             "hash": "Wv28tA951TqjtMLrTLkjj27ZCnw=",
             "jsonstr": null,
             "rewardBlockProposer": null,
             "rewardBlockTx": null,
             "rewardBlockAmount": 0
         }
         ]
         */
        fetch(global.api_url + `/rest/api/blockchain/block/list/0/25`)
            .then(result=>result.json())
            .then(items=>this.setState({items}))
    }

  render() {
    return (
        <div>
          <Container>
            {/*<Breadcrumbs routes={this.props.routes} params={this.props.params}  />*/}

            <Row className="show-grid">

              <Table responsive>
                <thead>
                <tr>
                  <th className="text-center">Height</th>
                  <th className="text-center">Timestamp</th>
                  <th className="text-center">Transactions</th>
                  <th className="text-center">Hash</th>
                  <th className="text-center">Proposer</th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.items.map(item =>
                        <tr key={item.id}>
                          <td className="text-center"><a href={'#/block/' + item.id}>{item.id}</a></td>
                          <td className="text-center">{dateFormat(new Date(item.time), "isoUtcDateTime")}</td>
                          <td className="text-right">{item.numTxs}</td>
                          <td className="text-right"><code>0x{global.base64toHexString(item.hash)}</code></td>
                          <td className="text-right"><code>0x{item.rewardBlockProposer}</code></td>
                        </tr>
                    )
                }
                </tbody>
              </Table>

            </Row>
          </Container>
        </div>
    )
  }
}

export default Blocks;
