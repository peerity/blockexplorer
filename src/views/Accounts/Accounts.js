import React, { Component } from 'react';
import {Container, Row, Col, Table} from 'reactstrap'
// import Breadcrumbs from 'react-breadcrumbs'

var dateFormat = require('dateformat');
var global = require("../../Global");

class Accounts extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: []
        };
    }

    componentDidMount(){
        fetch(global.api_url + `/rest/api/account/list/0/25`)
            .then(result=>result.json())
            .then(items=>this.setState({items}))
    }

  render() {
    return (
        <div>
          <Container>
            {/*<Breadcrumbs routes={this.props.routes} params={this.props.params}  />*/}

            <Row className="show-grid">

              <Table responsive>
                <thead>
                <tr>
                  <th className="text-center">Id</th>
                  <th className="text-center">pubKey</th>
                  <th className="text-center">balance</th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.items.map(item =>
                        <tr key={item.id}>
                          <td className="text-center"><a href={'#/account/' + item.id}>{item.id}</a></td>
                          <td className="text-right"><code>0x{item.pubKey}</code></td>
                          <td className="text-right">{item.balance}</td>
                        </tr>
                    )
                }
                </tbody>
              </Table>

            </Row>
          </Container>
        </div>
    )
  }
}

export default Accounts;
