import React, { Component } from "react";
// import {Grid, Row, Col, Panel, Table, Media} from 'react-bootstrap/lib'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  CardTitle,
	CardText,
	CardLink,
  Panel,
  Table,
  Media
} from "reactstrap";
// import Breadcrumbs from 'react-breadcrumbs'

import update from "immutability-helper";
var dateFormat = require("dateformat");
var global = require("../../Global");

export default class Block extends Component {
  constructor(props) {
    super(props);
    this.state = {
      block: {
        time: 0,
        hash: ""
      },
      txs: []
    };
  }

  componentDidMount() {
    /**
         {
            "id": 68,
            "time": 1491483118000,
            "numTxs": 1,
            "hash": "0RYEG+nkGj6ULo4UWIDGlfUb39I=",
            "jsonstr": ""
         }
         */
    fetch(
      global.api_url +
        `/rest/api/blockchain/block/` +
        this.props.match.params.blockHeight
    )
      .then(result => result.json())
      .then(block =>
        this.setState(update(this.state, { block: { $set: block } }))
      );

    /**
         [{
           "id": "032768C2BBDEB60B61D467E534800775FBA24E37",
           "type": "GroupCreateOperation",
           "operation": "eyJUeXBlIjoiR3JvdXBDcmVhdGVPcGVyYXRpb24iLCJPcGVyYXRpb24iOiI3QjIyNEU2MTZENjUyMjNBMjI0NzZDNkY2MjYxNkMyMjJDMjI0NDY1NzM2MzcyNjk3MDc0Njk2RjZFMjIzQTIyNDQ2NTY2NjE3NTZDNzQyMjJDMjI0Rjc3NkU2NTcyMjIzQTIyMzU0MTU1NTM2OTcyMzQ2OTY5Njc2QzY1MkYzNDMxNEE1MTRENUE3NTc4MzY1MTRCNzE3MDQ1M0QyMjJDMjI1MjY1NzE3NTY1NzM3NDczMjIzQTZFNzU2QzZDN0QiLCJTaWduYXR1cmUiOiJBQTA0NjYxQjEyRDg2QjBDRkIwMkFFRUJDNThCMzdBMDQwQzcwRTRBNzc3RjUyNUM2MTZDNzQzRTUyOEEzNDExOTk0REFFRjUzOTY0RDgyNjY5NUE3RDJDODlFMjhBNkQwMDcwRERBRkM3NDdEOTIyNTc0RTZCNUY4QkFDMTUwRSIsIlB1YmtleSI6IkVCM0I0MjA5MUVGNkMyRjhGQTk1MTMxOTk0MEMwMDNCRUM3QUFFMjMzNkJEMkFGQUJENkZCNTlFQjRBM0VGNkUiLCJGZWUiOjB9",
           "signature": "qgRmGxLYawz7Aq7rxYs3oEDHDkp3f1JcYWx0PlKKNBGZTa71OWTYJmlafSyJ4optAHDdr8dH2SJXTmtfi6wVDg==",
           "account":    {
              "pubKey": "EB3B42091EF6C2F8FA951319940C003BEC7AAE2336BD2AFABD6FB59EB4A3EF6E",
              "sequence": 1,
              "balance": 1000,
              "id": "E405128ABE228A095EFF8D4940C66EC7A40AAA91"
           },
           "fee": 0,
           "block":    {
              "id": 68,
              "time": 1491483118000,
              "numTxs": 1,
              "hash": "0RYEG+nkGj6ULo4UWIDGlfUb39I=",
              "jsonstr": "{\"block_meta\":{\"hash\":\"D116041BE9E41A3E942E8E145880C695F51BDFD2\",\"header\":{\"chain_id\":\"dawn-chain\",\"height\":68,\"time\":\"2017-04-06T12:51:58.256Z\",\"num_txs\":1,\"last_block_id\":{\"hash\":\"869F99E57FB61BA05D3040C8EE01C9D4B8FCD250\",\"parts\":{\"total\":1,\"hash\":\"EE1982448450250737A2D20987715180E125BDCA\"}},\"last_commit_hash\":\"DCC8E89AE9BC80AB3BEBDA03DB3284CF4E294FBC\",\"data_hash\":\"CAC376C2C1F348D869B46DC163D3A6C0F86F51C1\",\"validators_hash\":\"255D29756DE5E9AE28B39C9BAB5527557E1607D6\",\"app_hash\":\"5A3D0C76F6B485CA9A7B9937A7F3CC338643BEE3\"},\"parts_header\":{\"total\":1,\"hash\":\"EE15F1D4B592413E5AE711DC566ABA5954B462BB\"}},\"block\":{\"header\":{\"chain_id\":\"dawn-chain\",\"height\":68,\"time\":\"2017-04-06T12:51:58.256Z\",\"num_txs\":1,\"last_block_id\":{\"hash\":\"869F99E57FB61BA05D3040C8EE01C9D4B8FCD250\",\"parts\":{\"total\":1,\"hash\":\"EE1982448450250737A2D20987715180E125BDCA\"}},\"last_commit_hash\":\"DCC8E89AE9BC80AB3BEBDA03DB3284CF4E294FBC\",\"data_hash\":\"CAC376C2C1F348D869B46DC163D3A6C0F86F51C1\",\"validators_hash\":\"255D29756DE5E9AE28B39C9BAB5527557E1607D6\",\"app_hash\":\"5A3D0C76F6B485CA9A7B9937A7F3CC338643BEE3\"},\"data\":{\"txs\":[\"376232323534373937303635323233613232343737323666373537303433373236353631373436353466373036353732363137343639366636653232326332323466373036353732363137343639366636653232336132323337343233323332333434353336333133363434333633353332333233333431333233323334333733363433333634363336333233363331333634333332333233323433333233323334333433363335333733333336333333373332333633393337333033373334333633393336343633363435333233323333343133323332333433343336333533363336333633313337333533363433333733343332333233323433333233323334343633373337333634353336333533373332333233323333343133323332333333353334333133353335333533333336333933373332333333343336333933363339333633373336343333363335333234363333333433333331333434313335333133343434333534313337333533373338333333363335333133343432333733313337333033343335333334343332333233323433333233323335333233363335333733313337333533363335333733333337333433373333333233323333343133363435333733353336343333363433333734343232326332323533363936373665363137343735373236353232336132323431343133303334333633363331343233313332343433383336343233303433343634323330333234313435343534323433333533383432333333373431333033343330343333373330343533343431333733373337343633353332333534333336333133363433333733343333343533353332333834313333333433313331333933393334343434313435343633353333333933363334343433383332333633363339333534313337343433323433333833393435333233383431333634343330333033373330343434343431343634333337333433373434333933323332333533373334343533363432333534363338343234313433333133353330343532323263323235303735363236623635373932323361323234353432333334323334333233303339333134353436333634333332343633383436343133393335333133333331333933393334333034333330333033333432343534333337343134313435333233333333333634323434333234313436343134323434333634363432333533393435343233343431333334353436333634353232326332323436363536353232336133303764\"]},\"last_commit\":{\"blockID\":{\"hash\":\"869F99E57FB61BA05D3040C8EE01C9D4B8FCD250\",\"parts\":{\"total\":1,\"hash\":\"EE1982448450250737A2D20987715180E125BDCA\"}},\"precommits\":[{\"validator_address\":\"E405128ABE228A095EFF8D4940C66EC7A40AAA91\",\"validator_index\":0,\"height\":67,\"round\":0,\"type\":2,\"block_id\":{\"hash\":\"869F99E57FB61BA05D3040C8EE01C9D4B8FCD250\",\"parts\":{\"total\":1,\"hash\":\"EE1982448450250737A2D20987715180E125BDCA\"}},\"signature\":[1,\"F05B7D16018553CECC84AFCE8A2848F517BAF3EACCD2531280F637A86DC90B5633055E6F1689E8A8CE93A3F748D0986F32A8237009E8D1EF98F39532A89A400C\"]}]}}}"
           }
        }]
         */
    fetch(
      global.api_url +
        `/rest/api/blockchain/block/` +
        this.props.match.params.blockHeight +
        `/txs`
    )
      .then(result => result.json())
      .then(txs => this.setState(update(this.state, { txs: { $set: txs } })));
  }

  render() {
    return (
      <Container style={{a: {color: "hotPink"}}}>
        <Row>
          <Col>
            <Card inverse color="success">
              <CardHeader>
                <strong>Block Summary</strong>
              </CardHeader>
              <CardBlock>
                <dl className="dl-horizontal">
									<Row>
										<Col>
											<dt>Height</dt>
											<dd>
												{this.state.block.id}
											</dd>

											<dt>Timestamp</dt>
											<dd>
												{dateFormat(
													new Date(this.state.block.time),
													"isoUtcDateTime"
												)}
											</dd>

										</Col>
										<Col>
											<dt>Transactions</dt>
											<dd>
												{this.state.block.numTxs}
											</dd>
											<dt>Hash</dt>
											<dd>
												<code>
													0x{global.base64toHexString(this.state.block.hash)}
												</code>
											</dd>
										</Col>
										<Col>
											<dt>Previous Block</dt>
											<dd>
												<CardLink
													href={"/blockexplorer/block/" + (this.state.block.id - 1)}
												>
													{this.state.block.id - 1}
												</CardLink>
											</dd>

											<dt>Next Block</dt>
											<dd>
												<CardLink
													href={"/blockexplorer/block/" + (this.state.block.id + 1)}
												>
													{this.state.block.id + 1}
												</CardLink>
											</dd>
										</Col>
									</Row>
                </dl>
              </CardBlock>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card inverse color="primary">
              <CardHeader>
                <strong>Transactions</strong>
              </CardHeader>
              <CardBlock>
                {this.state.txs.map(item =>
                  <Media>
                    <Media left style={{marginRight: "30px"}}>
                      <Media
                        object
                        src="https://react-bootstrap.github.io/assets/thumbnail.png"
                        alt="Image"
                        style={{borderRadius: "50%"}}
                      />
                    </Media>
                    <Media body>
                      <Media>
                        <code>
                          0x{global.base64toHexString(item.id)}
                        </code>
                      </Media>
                      <p>
                        <b>{item.type}</b>, by <code>0x{item.account.id}</code>,{" "}
                        {item.fee} fee
                      </p>
                      <p>
                        <small>
                          Signature:{" "}
                          <code>
                            0x{global
                              .base64toHexString(item.signature)
                              .substring(0, 64)}...
                          </code>
                        </small>
                      </p>
                      <p>
                        <pre>
                          {JSON.stringify(
                            JSON.parse(
                              Buffer.from(item.operation, "base64").toString()
                            ),
                            null,
                            4
                          )}
                        </pre>
                      </p>
                    </Media>
                  </Media>
                )}
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
