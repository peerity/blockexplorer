// var crypto = require("tendermint-crypto");
// var hex = require("tendermint-crypto/src/hex");
// var Config = require('Config');

// http://www.tech-dojo.org/#!/articles/5697fd5ddb99acd646dea1aa
module.exports = {

    // getConfig () {
    //     return Config;
    // },

    api_url: "https://peerity.info",

    /**
     * Convert a hex string to a byte array
     * @param hex
     * @returns {Array}
     */
    hexToBytes(hex) {
        for (var bytes = [], c = 0; c < hex.length; c += 2)
            bytes.push(parseInt(hex.substr(c, 2), 16));
        return bytes;
    },

    /**
     * Convert a byte array to a hex string
     * @param bytes
     * @returns {string}
     */
    bytesToHex(bytes) {
        for (var hex = [], i = 0; i < bytes.length; i++) {
            hex.push((bytes[i] >>> 4).toString(16));
            hex.push((bytes[i] & 0xF).toString(16));
        }
        return hex.join("");
    },

    base64toHexString(strBase64) {
        return Buffer.from(strBase64, 'base64').toString('hex');
    },
}